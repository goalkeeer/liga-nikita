var dependencesOptions = createDependences(optionsDisableArray);

disableBannedOption(dependencesOptions);

$('.block-crossing .btn').on('click', function (e) {
    e.preventDefault();

    $(this).addClass('active').siblings().removeClass('active');
    var id = '#' + $(this).attr('href');

    $('.page').addClass('hidden');
    $(id).removeClass('hidden');
});

$("input[type='radio']").on('change', function () {
    var index = $(this).val();

    $('.left-block__inside .left-block__img.active').removeClass('active');
    $('.option-inside__item.active').removeClass('active');
    $(this).closest('label').addClass('active');
    $('.left-block__inside .left-block__img').eq(index).addClass('active');
});

$(document).on('click', '.option', function (e) {
    var $checkbox = $(':checkbox', this);
    var changeableOption = $(this).attr('data-option-num');


    if (e.target !== $checkbox[0] && !$checkbox.prop('disabled') ) {
        $checkbox.prop('checked', !$checkbox.prop('checked'));
        changeState(changeableOption);

        if ($checkbox.prop('checked') == true){
            addParagraph($checkbox);
        }else{
            deleteParagraph($checkbox);
        }
    }

    changeImage();
    disableBannedOption(dependencesOptions);
    changeTotal();
    setLastOptionMargin();
});

function disableBannedOption(dependences) {
    $('.option__input').prop( 'disabled', false);
    $('.option').removeClass('disabled');

    for ( var i = 0; i < state.length; i++){
        for ( var j = 0; j < dependences.length; j++){
            if ( dependences[j].id == i && dependences[j].value == state[i]){
                $('#' + dependences[j].disableId).prop( 'disabled', true );
                $(".option[data-option-num=" + dependences[j].disableId + "]").addClass('disabled');
            }
        }
    }
}

function createDependences(optionsDisableArray) {
    var dependencesArray = [];
    for ( var i = 0; i < optionsDisableArray.length -1 ; i++){
        for ( var j = 0; j < optionsDisableArray.length; j++){
            if ( optionsDisableArray[i][j] === 0 ){
                dependencesArray.push({
                    id: i,
                    value: false,
                    disableId: j
                    });
                dependencesArray.push({
                    id: j,
                    value: true,
                    disableId: i
                })
            }
        }
    }
    return dependencesArray;
}

function changeState(index) {
    if (state[index] === 1) {
        state[index] = 0;
    }else{
        state[index] = 1;
    }
}

function getIdImage() {
    var id = state.join('');
    id = id.slice(0, -1);
    return id;
}

function changeImage() {
    $('.left-block__outside .left-block__img.active').removeClass('active');
    $( "#"+getIdImage() ).addClass('active');
}

function changeTotal() {
    var total = 0;
    for ( var i = 0; i < state.length; i++){
        if (state[i] === 1) total += optionsPrice[i];
    }
    $('.total__text_num').text(total);
}

function addParagraph(elem) {
    var id = $(elem).attr('id');
    var title = $('.option[data-option-num="' + id + '"] .option__title').text();
    var cost = $('.option[data-option-num="' + id + '"] .option__price').text();

    $('.total__text').append('<div data-option-num="'+ id
        + '" class="total__paragraph clearfix"><span class="total__paragraph__name">' + title
        +'</span><span class="total__paragraph__cost">' + cost + '</span></div>');
}

function deleteParagraph(elem) {
    var id = $(elem).attr('id');
    $('.total__paragraph[data-option-num="' + id + '"]').remove();
}

function setLastOptionMargin() {
    var totalHeigth = $('.total').outerHeight();
    var lastOption = $('#outside .option:nth-last-child(1)');
    lastOption.css('margin-bottom', totalHeigth + 'px');
}
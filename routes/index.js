var express = require('express');
var router = express.Router();
var entrance = require('../models');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Liga-stand', backgroundImage: entrance.outside.mainBackgroundImage });
});

router.get('/designer', function (req, res, next) {
  res.render('designer', { title: 'Конструктор', entrance: entrance});
});

module.exports = router;
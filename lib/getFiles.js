var fs = require('fs');
var path = require('path');

var getFiles = function (dir, extension, files){
    if ( extension === undefined) {
        extension = '\.';
    }else extension = '\.' + extension + '$';

    files = files || [];
    var readFiles = fs.readdirSync('.//public' + dir);

    for (var i in readFiles){
        var path = dir + '/' + readFiles[i];

        if (fs.statSync( './/public' + path).isDirectory()){
            getFiles(path, extension, files);
        } else {
            if ( path.match(extension) ){
                var name = readFiles[i];
                name = name.slice( 0, name.lastIndexOf('.') );
                files.push( { "path": path, "name": name } );
            }
        }
    }
    return files;
};

module.exports = getFiles;
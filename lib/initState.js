var getFiles = require("./getFiles");
var fs = require('fs');

var files = getFiles("/images/outside", "jpg");
var amountOptions = files[0].name.length;
var options = fs.readFileSync('.//public/images/options.json', 'utf8');
options = JSON.parse(options);

var state = [0];

checkAmountOptions(files, amountOptions);
checkAmountOptionsDescription(options, amountOptions);
pushState(amountOptions);

function checkAmountOptions (files, amountOptions) {
    for ( var i = 0; i < files.length; i++ ){
        if ( amountOptions != files[i].name.length ) {
            var error = new Error('different quantity of options');
            error.name = 'Error options';
            throw error;
        }
    }
}

function checkAmountOptionsDescription (options, amountOptions) {
    var amountOptionsDescription = options.outside.length;
    if ( amountOptions != amountOptionsDescription){
        var error = new Error('discrepancy of quantity of options with the description');
        error.name = 'Error options';
        throw error;
    }
}

function pushState (amountOptions) {
    for (var i = 0; i < amountOptions; i++){
        state.push(0);
    }
}

module.exports = state;
var fs = require('fs');

var options = fs.readFileSync('.//public/images/options.json', 'utf8');
options = JSON.parse(options);
var optionsPrice = [];

pushStatePrice(options, optionsPrice);

function pushStatePrice (options, optionsPrice) {
    for ( var i = 0; i < options.outside.length; i++){
        optionsPrice.push(options.outside[i].price);
    }
    optionsPrice.push(options.inside.price);
}

module.exports = optionsPrice;
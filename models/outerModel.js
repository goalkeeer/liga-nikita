var fs = require('fs');
var getFiles = require('../lib/getFiles');

var outsideImages = getFiles('/images/outside', 'jpg');
var mainBackgroundImage = outsideImages[outsideImages.length - 1];

var options = fs.readFileSync('.//public/images/options.json', 'utf8');
options = JSON.parse(options);
var outsideOptions = options.outside;
var disable = options.disable;

var outside = {
    images: outsideImages,
    mainBackgroundImage: mainBackgroundImage,
    options: outsideOptions,
    disable: disable
};

module.exports = outside;
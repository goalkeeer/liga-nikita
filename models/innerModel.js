var fs = require('fs');
var getFiles = require('../lib/getFiles');

var insideImages = getFiles('/images/inside', 'jpg');

var options = fs.readFileSync('.//public/images/options.json', 'utf8');
options = JSON.parse(options);
var insideOptions = options.inside;

var inside = {
    images: insideImages,
    options: insideOptions
};

module.exports = inside;
var inside = require('./innerModel');
var outside = require('./outerModel');
var state = require('../lib/initState');
var optionsPrice = require('../lib/initPrice');

var entrance = {
    inside: inside,
    outside: outside,
    state: state,
    price: optionsPrice
};

module.exports = entrance;